# CHIP-8 Emulator

A CHIP-8 virtual machine emulator written in c++ based on [this](http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/) tutorial.