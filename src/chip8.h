#ifndef CHIP8_H
#define CHIP8_H

#include <cstdint>

class Chip8 {
private:
	// Current opcode and 4K memory
	uint16_t opcode;
	uint8_t memory[4096];

	// Registers and program counter
	uint8_t V[16];
	uint16_t I;
	uint16_t pc;

	// The stack
	uint16_t stack[16];
	uint16_t sp;

	// Timers
	uint8_t delay_timer;
	uint8_t sound_timer;
public:
	// Graphics and Key input
	uint8_t gfx[64 * 32];
	uint8_t* keys;
	bool draw_flag;

	void initialize();
	void load_program(const char *filepath);
	void emulate_cycle();
};

#endif