#include "keypad.h"
#include <iostream>

Keypad::Keypad() {
	if (SDL_Init(SDL_INIT_EVENTS) != 0) {
		std::cout << "SDL couldn't init events:" << SDL_GetError() << std::endl;
		exit(1);
	}

	for (int i = 0; i < 16; ++i) {
		keys[i] = 0;
	}
}

Keypad::~Keypad() {
	SDL_Quit();
}

uint8_t* Keypad::get_keys() {
	SDL_PollEvent(&event);

	switch (event.type) {
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
				case SDLK_1:
					keys[1] = 0;
					break;
				case SDLK_2:
					keys[2] = 0;
					break;
				case SDLK_3:
					keys[3] = 0;
					break;
				case SDLK_4:
					keys[12] = 0;
					break;
				case SDLK_q:
					keys[4] = 0;
					break;
				case SDLK_w:
					keys[5] = 0;
					break;
				case SDLK_e:
					keys[6] = 0;
					break;
				case SDLK_r:
					keys[13] = 0;
					break;
				case SDLK_a:
					keys[7] = 0;
					break;
				case SDLK_s:
					keys[8] = 0;
					break;
				case SDLK_d:
					keys[9] = 0;
					break;
				case SDLK_f:
					keys[14] = 0;
					break;
				case SDLK_z:
					keys[10] = 0;
					break;
				case SDLK_x:
					keys[0] = 0;
					break;
				case SDLK_c:
					keys[11] = 0;
					break;
				case SDLK_v:
					keys[15] = 0;
					break;
				default:
					break;
			}
			break;
		case SDL_KEYUP:
			switch (event.key.keysym.sym) {
				case SDLK_1:
					keys[1] = 1;
					break;
				case SDLK_2:
					keys[2] = 1;
					break;
				case SDLK_3:
					keys[3] = 1;
					break;
				case SDLK_4:
					keys[12] = 1;
					break;
				case SDLK_q:
					keys[4] = 1;
					break;
				case SDLK_w:
					keys[5] = 1;
					break;
				case SDLK_e:
					keys[6] = 1;
					break;
				case SDLK_r:
					keys[13] = 1;
					break;
				case SDLK_a:
					keys[7] = 1;
					break;
				case SDLK_s:
					keys[8] = 1;
					break;
				case SDLK_d:
					keys[9] = 1;
					break;
				case SDLK_f:
					keys[14] = 1;
					break;
				case SDLK_z:
					keys[10] = 1;
					break;
				case SDLK_x:
					keys[0] = 1;
					break;
				case SDLK_c:
					keys[11] = 1;
					break;
				case SDLK_v:
					keys[15] = 1;
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}

	return keys;
}