#ifndef KEYPAD_H
#define KEYPAD_H

#include <SDL2/SDL.h>

class Keypad {
private:
	SDL_Event event;
	uint8_t keys[16];
public:
	Keypad();
	~Keypad();
	uint8_t* get_keys();
};

#endif
