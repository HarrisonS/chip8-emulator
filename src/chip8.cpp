#include "chip8.h"
#include <iostream>

uint8_t fontset[80] = {
	0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
	0x20, 0x60, 0x20, 0x20, 0x70, // 1
	0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
	0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
	0x90, 0x90, 0xF0, 0x10, 0x10, // 4
	0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
	0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
	0xF0, 0x10, 0x20, 0x40, 0x40, // 7
	0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
	0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
	0xF0, 0x90, 0xF0, 0x90, 0x90, // A
	0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
	0xF0, 0x80, 0x80, 0x80, 0xF0, // C
	0xE0, 0x90, 0x90, 0x90, 0xE0, // D
	0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
	0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

void Chip8::initialize() {
	opcode = 0;
	I = 0;
	pc = 0x200;
	sp = 0;

	for (int i = 0; i < 4096; ++i) {
		memory[i] = 0;
	}

	keys = new uint8_t[16];
	for (int i = 0; i < 16; ++i) {
		V[i] = 0;
		stack[i] = 0;
		keys[i] = 0;
	}

	for (int i = 0; i < 2048; ++i) {
		gfx[i] = 0;
	}

	for (int i = 0; i < 80; ++i) {
		memory[i] = fontset[i];
	}

	delay_timer = 0;
	sound_timer = 0;
}

void Chip8::load_program(const char *filepath) {
	FILE *fp;
	fp = fopen(filepath, "rb");
	if (fp == NULL) {
		std::cout << "Couldn't open file: " << filepath << std::endl;
		exit(1);
	}

	fseek(fp, 0L, SEEK_END);
	int fsize = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	uint8_t *buffer = &memory[512];
	fread(buffer, fsize, 1, fp);
}

void unimplemented_opcode(uint16_t opcode) {
	std::cout << "Unimplemented opcode: " << std::hex << opcode << "\n";
	exit(1);
}

void Chip8::emulate_cycle() {
	opcode = memory[pc] << 8 | memory[pc + 1];

	switch (opcode & 0xF000) {
		case 0x0000:
			switch (opcode & 0x000F) {
				case 0x0000: // Display clear
					unimplemented_opcode(opcode);
					break;
				case 0x000E: // Return from subroutine
					pc = stack[sp];
					--sp;
					break;
				default:
					std::cout << "Error, unkown [0x0000] opcode: " << std::hex << opcode << "\n";
					exit(1);
			}
			break;
		case 0x1000: // 0x1NNN - Jump to address NNN
			pc = (opcode & 0x0FFF);
			break;
		case 0x2000: // 0x2NNN - Call address NNN
			++sp;
			pc += 2;
			stack[sp] = pc;
			pc = (opcode & 0x0FFF);
			break;
		case 0x3000: // 0x3XNN - Skip next instruction if Vx==NN
			if (V[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF)) {
				pc += 2;
			}
			pc += 2;
			break;
		case 0x4000: // 0x4XNN - Skip next instruction if Vx!=NN
			if (V[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF)) {
				pc += 2;
			}
			pc += 2;
			break;
		case 0x5000: // 0x5XY0 - Skip next instruction if Vx==Vy
			if (V[(opcode & 0x0F00) >> 8] == V[(opcode & 0x00F0) >> 4]) {
				pc += 2;
			}
			pc += 2;
			break;
		case 0x6000: // 0x6XNN - Sets Vx to NN
			V[(opcode & 0x0F00) >> 8] = (opcode & 0x00FF);
			pc += 2;
			break;
		case 0x7000: // 0x7XNN - Adds NN to Vx
			V[(opcode & 0x0F00) >> 8] += (opcode & 0x00FF);
			pc += 2;
			break;
		case 0x8000:
			switch (opcode & 0x000F) {
				case 0x0000: // 0x8XY0 - Sets Vx to Vy
					V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4];
					pc += 2;
					break;
				case 0x0001: // 0x8XY1 - Sets Vx to Vx | Vy
					V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x0F00) >> 8] | V[(opcode & 0x00F0) >> 4];
					pc += 2;
					break;
				case 0x0002: // 0x8XY2 - Sets Vx to Vx & Vy
					V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x0F00) >> 8] & V[(opcode & 0x00F0) >> 4];
					pc += 2;
					break;
				case 0x0003: // 0x8XY3 - Set Vx to Vx ^ Vy
					V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x0F00) >> 8] ^ V[(opcode & 0x00F0) >> 4];
					pc += 2;
					break;
				case 0x0004: // 0x8XY4 - Add Vy to Vx, set Vf to 1 if carry
					if (V[(opcode & 0x00F0) >> 4] > (0xFF - V[(opcode & 0x0F00) >> 8])) {
						V[0xF] = 1;
					} else {
						V[0xF] = 0;
					}
					V[(opcode & 0x0F00) >> 8] += V[(opcode & 0x00F0) >> 4];
					pc += 2;
					break;
				case 0x0005: // 0x8XY5 - Vy is subracted from Vx, set Vf to 0 if borrow
					if (V[(opcode & 0x0F00) >> 8] < V[(opcode & 0x00F0) >> 4]) {
						V[0xF] = 0;
					} else {
						V[0xF] = 1;
					}
					V[(opcode & 0x0F00) >> 8] -= V[(opcode & 0x00F0) >> 4];
					pc += 2;
					break;
				case 0x0006: // 0x8XY6 - Store least sig bit of Vx in Vf then shift right 1
					V[0xF] = (V[(opcode & 0x0F00) >> 8] & 0x0001);
					V[(opcode & 0x0F00) >> 8] >>= 1;
					pc += 2;
					break;
				case 0x0007: // 0x8XY7 - Set Vx to Vy - Vx, set Vf to 0 if borrow
					if (V[(opcode & 0x0F00) >> 8] > V[(opcode & 0x00F0) >> 4]) {
						V[0xF] = 0;
					} else {
						V[0xF] = 1;
					}
					V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4] - V[(opcode & 0x0F00) >> 8];
					pc += 2;
					break;
				case 0x000E: // 0x8XYE - Store most sig bit of Vx in Vf then shift left 1
					V[0xF] = (V[(opcode & 0x0F00) >> 8] & 0x0080);
					V[(opcode & 0x0F00) >> 8] <<= 1;
					pc += 2;
					break;
				default:
					std::cout << "Error, unkown [0x8000] opcode: " << std::hex << opcode << "\n";
					exit(1);
			}
			break;
		case 0x9000: // 0x9XY0 - Skip next instruction if Vx != Vy
			if (V[(opcode & 0x0F00) >> 8] != V[(opcode & 0x00F0) >> 4]) {
				pc += 2;
			}
			pc += 2;
			break;
		case 0xA000: // 0xANNN - Sets I to address NNN
			I = (opcode & 0x0FFF);
			pc += 2;
			break;
		case 0xB000: // 0xBNNN - Jump to address NNN + V0
			pc = (opcode & 0x0FFF) + V[0];
			break;
		case 0xC000: // 0xCXNN - Set Vx to & of random number and NN
			V[(opcode & 0x0F00) >> 8] = (rand() % 256) & (opcode & 0x00FF);
			pc += 2;
			break;
		case 0xD000: { // 0xDXYN - Draw sprite at Vx, Vy with height N
			uint16_t x = V[((opcode & 0x0F00) >> 8)];
			uint16_t y = V[((opcode & 0x00F0) >> 4)];
			uint16_t height = (opcode & 0x000F);
			uint16_t pixel;

			V[0xF] = 0;
			for (int yline = 0; yline < height; ++yline) {
				pixel = memory[I + yline];
				for (int xline = 0; xline < 8; ++xline) {
					if ((pixel & (0x80 >> xline)) != 0) {
						if (gfx[(x + xline + ((y + yline) * 64))] == 1) {
							V[0xF] = 1;
						}
						gfx[x + xline + ((y + yline) * 64)] ^= 1;
					}
				}
			}
			draw_flag = true;
			pc += 2;
			break;
		} case 0xE000:
			switch (opcode & 0x000F) {
				case 0x0001: // 0xEXA1 - Skip next instruction if key in Vx isn't pressed
					if (keys[V[(opcode & 0x0F00) >> 8]] != 0) {
						pc += 2;
					}
					pc += 2;
					break;
				case 0x000E: // 0xEX9E - Skip next instruction if key in Vx is pressed
					if (keys[V[(opcode & 0x0F00) >> 8] == 0]) {
						pc += 2;
					}
					pc += 2;
					break;
				default:
					std::cout << "Error, unkown [0xE000] opcode: " << std::hex << opcode << "\n";
					exit(1);
			}
			break;
		case 0xF000:
			switch (opcode & 0x00FF) {
				case 0x0007: // 0xFX07 - Set Vx to value of delay timer
					V[(opcode & 0x0F00) >> 8] = delay_timer;
					pc += 2;
					break;
				case 0x000A: // 0xFX0A - Key press awaited then stored in Vx
					unimplemented_opcode(opcode);
					break;
				case 0x0015: // 0xFX15 - Set delay timer to Vx
					delay_timer = V[(opcode & 0x0F00) >> 8];
					pc += 2;
					break;
				case 0x0018: // 0xFX18 - Set sound timer to Vx
					sound_timer = V[(opcode & 0x0F00) >> 8];
					pc += 2;
					break;
				case 0x001E: // 0xFX1E - add Vx to I, set Vf if there is a range overflow
					if ((I + V[(opcode & 0x0F00) >> 8]) > 0xFFF) {
						V[0xF] = 1;
					} else {
						V[0xF] = 0;
					}
					I += V[(opcode & 0x0F00) >> 8];
					pc += 2;
					break;
				case 0x0029: // 0xFX29 - Set I to sprite for char in Vx
					//I = fontset[V[(opcode & 0x0F00) >> 8] * 5];
					I = V[(opcode & 0x0F00) >> 8] * 5;
					pc += 2;
					break;
				case 0x0033: { // 0xFX33 - BCD
					int bcd = V[(opcode & 0x0F00) >> 8];
					memory[I] = bcd / 100;
					memory[I + 1] = (bcd % 100) / 10;
					memory[I + 2] = (bcd % 10);
					pc += 2;
					break;
				} case 0x0055: // 0xFX55 - Store V0 to Vx in memory starting at I
					for (int i = 0; i <= (opcode & 0x0F00) >> 8; ++i) {
						memory[I+i] = V[i];
					}
					pc += 2;
					break;
				case 0x0065: // 0xFX65 - Fill V0 to Vx from memory starting at I
					for (int i = 0; i <= (opcode & 0x0F00) >> 8; ++i) {
						V[i] = memory[I + i];
					}
					pc += 2;
					break;
				default:
					std::cout << "Error, unkown [0xF000] opcode: " << std::hex << opcode << "\n";
					exit(1);
			}
			break;
		default:
			std::cout << "Error, opcode not recognized: " << std::hex << opcode << "\n";
			exit(1);
	}

	if (delay_timer > 0) {
		--delay_timer;
	}

    // TODO: implement actual sound
	if (sound_timer > 0) {
		std::cout << "Beep!\n";
		--sound_timer;
	}
}
