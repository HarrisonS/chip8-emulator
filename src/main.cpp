#include "chip8.h"
#include "display.h"
#include "keypad.h"
#include <iostream>
#undef main

int main() {
	// Setup graphics and input
	Display display = Display();
	Keypad keypad = Keypad();
	
	// Initialize cpu and load game
	Chip8 chip8 = Chip8();
	chip8.initialize();
	chip8.load_program("breakout"); // TODO: swap for argument based file selection

	while (!display.quit) {
		chip8.emulate_cycle();

		// Update keys
		chip8.keys = keypad.get_keys();

		// Update and draw display
		display.update();
		if (chip8.draw_flag) {
			display.draw_buffer(chip8.gfx, 64, 32);
			chip8.draw_flag = false;
		}
	}

	return 0;
}
