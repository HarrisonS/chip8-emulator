#include "display.h"
#include <iostream>
#include <cstring>

Display::Display() {
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL couldn't init video:" << SDL_GetError() << std::endl;
		exit(1);
	}

	window = SDL_CreateWindow("Chip8 Emulator", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_ALLOW_HIGHDPI);
	if (window == NULL) {
		std::cout << "Error creating window:" << SDL_GetError() << std::endl;
		exit(1);
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == NULL) {
		std::cout << "Errow creating renderer:" << SDL_GetError() << std::endl;
		exit(1);
	}

	texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, WIDTH, HEIGHT);
	pixels = new uint32_t[WIDTH * HEIGHT];
	memset(pixels, 0, WIDTH * HEIGHT * sizeof(uint32_t));

	quit = false;
}

Display::~Display() {
	delete[] pixels;
	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void Display::update() {
	if (SDL_PollEvent(&event)) {
		switch (event.type) {
			case SDL_QUIT:
				quit = true;
				break;
		}
	}
}

void Display::draw_buffer(uint8_t *buffer, int w, int h) {
	int sw = WIDTH / w;
	int sh = HEIGHT / h;
	for (int y = 0; y < HEIGHT; ++y) {
		for (int x = 0; x < WIDTH; ++x) {
			pixels[(y * WIDTH) + x] = buffer[(y / sh) * w + (x / sw)]*0xFFFFFF;
		}
	}

	SDL_UpdateTexture(texture, NULL, pixels, WIDTH * sizeof(uint32_t));
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, texture, NULL, NULL);
	SDL_RenderPresent(renderer);
}
