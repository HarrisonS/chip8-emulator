#ifndef DISPLAY_H
#define DISPLAY_H

#include <SDL2/SDL.h>

const int WIDTH = 640, HEIGHT = 320;

class Display {
private:
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Texture *texture;
	SDL_Event event;
	uint32_t *pixels;
public:
	bool quit;
	Display();
	~Display();
	void update();
	void draw_buffer(uint8_t *buffer, int w, int h);
};

#endif
