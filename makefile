CXX=g++
EXE = CHIP8Emu

SRC_DIR = src
OBJ_DIR = build

SRC = $(wildcard $(SRC_DIR)/*.cpp)
OBJ = $(SRC:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)

CPPFLAGS += -I.
CFLAGS += -Wall
LDFLAGS += -lmingw32 -lSDL2main -lSDL2 -I E:\Programs\SDL2_MinGW\include
LDLIBS += -L E:\Programs\SDL2_MinGW\lib

.PHONY: all clean

all: $(EXE)

$(EXE): $(OBJ)
	$(CXX) $^ -o $@ $(LDLIBS) $(LDFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CPPFLAGS) $(CFLAGS) -c $< -o $@
